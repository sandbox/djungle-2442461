-- SUMMARY --

A tiny extension for Easy Social module that adds a Send email button.

-- REQUIREMENTS --

Easy Social module, https://www.drupal.org/project/easy_social

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.
